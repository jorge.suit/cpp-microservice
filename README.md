In this service we want to implement a REST service in C++:
* POST /api/xslt/name=OrderRQ&from=17.2&to=18.2
* the body is an XML
* the response is the xml transformed

The first approach will response the body without transformation.

We can do the implementation based on https://github.com/Microsoft/cpprestsdk

## Configuring

Just change the location where vcpkg has the 
```
mkdir -p build/debug
cd build/debug
cmake ../.. -DCMAKE_TOOLCHAIN_FILE=/home/jorge/sources/cxx/vcpkg/scripts/buildsystems/vcpkg.make -DCMAKE_BUILD_TYPE=Debug
```
